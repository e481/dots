
alias ls="ls --color=auto"
alias la="ls -la --color=auto"
alias ll="ls -l --color=auto"
alias gst="git status"
alias gco="git checkout"
alias gcam="git commit -a -m"
alias gd="git diff"
alias vim="nvim"

